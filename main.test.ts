import { assertEquals } from 'https://deno.land/std@0.136.0/testing/asserts.ts'
import { startCalculation } from './main.ts'

Deno.test('Calculate nothing #1', () => {
  const result = startCalculation(5).calculate()
  assertEquals(result, 5)
})

Deno.test('Add one to calculation #1', () => {
  const result = startCalculation(2).addOne().calculate()
  assertEquals(result, 3)
})

Deno.test('Add two to calculation #1', () => {
  const result = startCalculation(2).addTwo().calculate()
  assertEquals(result, 4)
})

Deno.test('Add custom value to calculation #1', () => {
  const result = startCalculation(8).add(100).calculate()
  assertEquals(result, 108)
})

Deno.test('Divide calculation by four #1', () => {
  const result = startCalculation(8).divideByFour().calculate()
  assertEquals(result, 2)
})

Deno.test('Tap calculation #1', () => {
  const result = startCalculation(10)
    .tap((value) => assertEquals(value, 10))
    .addOne()
    .tap((value) => assertEquals(value, 11))
    .add(10)
    .tap((value) => assertEquals(value, 21))
    .add(100)
    .calculate()

  assertEquals(result, 121)
})

Deno.test('Remove last operation #1', () => {
  const result = startCalculation(10)
    .addOne()
    .removeLastOperation()
    .calculate()

  assertEquals(result, 10)
})

Deno.test('Remove last operation #2', () => {
  const result = startCalculation(25)
    .addOne()
    .tap((value) => assertEquals(value, 26))
    .removeLastOperation()
    .calculate()

  assertEquals(result, 25)
})

Deno.test('All methods chained together #1', () => {
  const result = startCalculation(400)
    .addTwo()
    .tap((currentValue) => assertEquals(currentValue, 402))
    .removeLastOperation()
    .addOne()
    .tap((currentValue) => assertEquals(currentValue, 401))
    .add(400)
    .tap((currentValue) => assertEquals(currentValue, 801))
    .divideByFour()
    .calculate()

  assertEquals(result, 200.25)
})
