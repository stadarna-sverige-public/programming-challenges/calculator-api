# Implement calculator API 🧮

Test your coding skills!

## Prerequisites

- You need to have [Deno](https://deno.land/) installed to run the tests

## Task

First off, clone this repo!

You're implementing a calculator library. Your library consumers expects this API:

```typescript
console.log(
	// 400 will be the value to start from
  startCalculation(400)
		// Adds two to the previous value
    .addTwo()
		// Tap allows the consumer to cause a side effect with the current value of the calculation
    .tap((currentValue) =>
      console.log(`current value is ${currentValue}`) // outputs: current value is 402
    )
		// Will remove the last operation from the calculation, in this case adding two will be left out from here
    .removeLastOperation()
		// Adds one to the previous value
    .addOne()
    .tap((currentValue) =>
      console.log(`current value is ${currentValue}`) // outputs: current value is 401
    )
		// Add a custom value to the previous value
    .add(400)
		// Divide current value by four
    .divideByFour()
		// Calculates the result and returns it
    .calculate()
) // outputs: 200.25
```

You only have to implement the methods that the consumers has stated that they want, but it is of course preferable to do it in a way that allows for simple additions of methods later.

## Rules

- It is not allowed to add dependencies. This problem does not need other libraries

## Tips

- You have tests included. Just run the command `deno test` in the project directory and you will see if your implementation holds
- If you want to run code in the file `main.ts` you run the command `deno run main.ts` in the project directory